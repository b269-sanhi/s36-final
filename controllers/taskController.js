// Contains instructions on HOW your API will perform its intended tasks
// All operations in can do will be placed in this file

const Task = require("../models/task")

// Controller function for gettings all the task
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}

// Controller function for creating a task
module.exports.createTask = (requestBody) => {
		let newTask = new Task({
			name: requestBody.name
		})

		return newTask.save().then((task, error) => {
			if (error){
				console.log(error)
				return false
			}else{
				return task
			}
		})
}

// Controller function for deleting a task
/*
Business Logic
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((returnTask, err) =>{
		if(err){
			console.log(err)
			return false
		}else{
			return "Deleted Task"
		}
	})
}

// [SECTION] ACTIVITY
// GET TASK
module.exports.GetTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result
	})
}

// UPDATE A TASK


module.exports.updateTask = (taskId) => { 	
	return Task.findByIdAndUpdate(taskId, {status: "completed"}).then((updatedTask, err) => { 		
		if (err){ 
			console.log(err)		
		return false; 	
	} else
		 { 			return updatedTask
		} 	
	})
	 }
	




