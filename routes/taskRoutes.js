// Defines WHEN particular controllers will be used
// Contain all the endpoints and response that we can get from controllers

const express = require("express")
// Creates a router instance that function as a middleware and routing system
const router = express.Router()

const taskController = require("../controllers/taskController")

// Route to get all the task
//  http://localhost:3001/tasks
router.get("/", (req,res) =>{
	taskController.getAllTasks().then(resultFromController =>
		res.send(resultFromController))
})


// Route to create task

router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController =>
		res.send(resultFromController))
})

// Route to delete task
router.delete("/:id", (req,res) => {
	taskController.deleteTask(req.params.id).then(resultFromController =>
		res.send(resultFromController))
})


// [SECTION] ACTIVITY
// GET TASK
router.get("/:id", (req,res) => {
	taskController.GetTask(req.params.id).then(resultFromController =>
		res.send(resultFromController))
})

// UPDATE A TASK

router.put("/:id/complete", (req,res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});



module.exports = router

// router.put("/:id", (req, res) => {

// 	// The "updateTask" function will accept the following 2 arguments:
// 		// "req.params.id" retrieves the task ID from the parameter
// 		// "req.body" retrieves the data of the updates that will be applied to a task from the request's "body" property
// 	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
// });



// // Use "module.exports" to export the router object to use in the "index.js"
// module.exports = router;

